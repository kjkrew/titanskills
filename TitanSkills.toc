## Interface: 80200
## Title: Titan Panel [|cffbc1212Skills|r] |cff00aa00<%version%>|r
## Notes: Displays Professions on the Titan Panel
## Author: Kjasi
## URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-URL: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Website: https://bitbucket.org/Kjasi/kjasis-wow-addons
## X-Feedback: https://bitbucket.org/Kjasi/kjasis-wow-addons
## Dependencies: Titan
## Version: <%version%>
Localize.lua
skills.lua
skills.xml